/*
1. In the S24 folder, create an a1 folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.


3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

// Template Literals
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.

// Object Destructuring
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.

// Arrow Functions
9. Create an array of numbers.

// A
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

// B
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

// Javascript Objects
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.

14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s24 activity code".
16. Add the link in Boodle.
*/

// Creating a variable getCube and use the exponent operator to compute for the cube of a number
let getCube = (num) => {
return num ** 3;
};

// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
let number = 5;
console.log(`The cube of ${number} is ${getCube(number)}.`);

// Creating a variable address with a value of an array containing details of an address.
let address = [
"148-4 Micheon-ri, Munui-myeon",
"청주시",
"충청북도",
"28208"
];

// Destructuring the array and print out a message with the full address using Template Literals
let [street, city, state, zipCode] = address;
console.log(`I live at ${street}, ${city}, ${state} ${zipCode}.`);

// Creating a variable animal with a value of an object data type with different animal details as it’s properties
let animal = {
type: "dog",
breed: "Labrador Retriever",
age: 5,
weight: "75 lbs"
};

// Destructure the object and print out a message with the details of the animal using Template Literals
let { type, breed, age, weight } = animal;
// If dot notation,
	// console.log(`The ${animal.type} is a ${animal.breed} and is ${animal.age} years old, weighing ${weight}.`);
console.log(`The ${type} is a ${breed} and is ${age} years old, weighing ${weight}.`);

// Creating an array of numbers
let numbers = [1, 2, 3, 4, 5];

// Looping through the array using forEach, an arrow function and using the implicit return statement to print out the numbers
numbers.forEach(num => console.log(num));

// Creating a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array
let reduceNumber = numbers.reduce((sum, num) => sum + num, 0);
console.log(`The sum of the numbers in the array is ${reduceNumber}.`);

// Creating a class of a Dog and a constructor that will accept a name, age and breed as it’s properties
class Dog {
constructor(name, age, breed) {
this.name = name;
this.age = age;
this.breed = breed;
}
}

// Creating/instantiating a new object from the class Dog and console log the object
let dog = new Dog("Pyro", 2, "American Pit Bull Terrier Dog");
console.log(dog);